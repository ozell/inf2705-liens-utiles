function resultatSucces(span1, span2, msg = "")
{
    document.getElementById(span1).innerHTML = "Succès :";
    document.getElementById(span1).className = "green";
    document.getElementById(span2).innerHTML = msg;
    document.getElementById(span2).className = "green";
}
function resultatEchec(span1, span2, msg = "")
{
    document.getElementById(span1).innerHTML = "Échec :";
    document.getElementById(span1).className = "red";
    document.getElementById(span2).innerHTML = msg;
    document.getElementById(span2).className = "red";
}
function resultatDesactive(span1, span2, msg = "")
{
    document.getElementById(span1).innerHTML = "(désactivé)";
    document.getElementById(span1).className = "";
    document.getElementById(span2).innerHTML = msg;
    document.getElementById(span2).className = "";
}
function resultatNonApplicable(span1, span2, msg = "")
{
    document.getElementById(span1).innerHTML = "(non appliqué)";
    document.getElementById(span1).className = "";
    document.getElementById(span2).innerHTML = msg;
    document.getElementById(span2).className = "";
}

function clamp(v,vmin,vmax)
{
    return( v < vmin ? vmin : v > vmax ? vmax : v );
}

function calculerPixel()
{
    // les attributs du fragment courant
    var zFrag = document.getElementById('zFrag').value;
    var rFrag = document.getElementById('rFrag').value;
    var gFrag = document.getElementById('gFrag').value;
    var bFrag = document.getElementById('bFrag').value;
    var aFrag = document.getElementById('aFrag').value;
    // les valeurs dans le tampon
    var zTamp = document.getElementById('zTamp').value;
    var rTamp = document.getElementById('rTamp').value;
    var gTamp = document.getElementById('gTamp').value;
    var bTamp = document.getElementById('bTamp').value;
    var aTamp = document.getElementById('aTamp').value;
    var sTamp = document.getElementById('sTamp').value;
    // les valeurs courantes
    var zCour = zFrag;
    var rCour = rFrag;
    var gCour = gFrag;
    var bCour = bFrag;
    var aCour = aFrag;
    var sCour = sTamp;

    var deviendraPixel = true; // si aucun test n'est actif, alors le fragment deviendra un pixel

    //
    // test du stencil
    //
    var stencilSucces = true; // par défaut
    var stencilEnabled = document.getElementById('stencilEnabled').value;
    document.getElementById('stencilfunc').disabled = !( stencilEnabled === "Enable" );
    document.getElementById('ref').disabled = !( stencilEnabled === "Enable" );
    document.getElementById('mask').disabled = !( stencilEnabled === "Enable" );
    document.getElementById('sfail').disabled = !( stencilEnabled === "Enable" );
    document.getElementById('zfail').disabled = !( stencilEnabled === "Enable" );
    document.getElementById('pass').disabled = !( stencilEnabled === "Enable" );
    if ( stencilEnabled === "Enable" )
    {
        if ( deviendraPixel )
        {
            var stencilfunc = document.getElementById('stencilfunc').value;
            var ref = document.getElementById('ref').value;
            var mask = document.getElementById('mask').value;
            document.getElementById('ref').disabled = ( stencilfunc === "NEVER" || stencilfunc === "ALWAYS" );
            document.getElementById('mask').disabled = ( stencilfunc === "NEVER" || stencilfunc === "ALWAYS" );
	    var raison = ""; // la raison du succès ou de l'insuccès du test
	    function msgFunc(test = "") { return "((ref&mask) "+test+" (stencil&mask)) &nbsp;&rArr;&nbsp; (("+ref+"&"+mask+") "+test+" ("+sTamp+"&"+mask+")) &nbsp;&rArr;&nbsp; ("+(ref & mask)+" "+test+" "+(sTamp & mask)+")"; }
            switch ( stencilfunc ) {
            case "NEVER":    stencilSucces = false; raison = "jamais"; break;
            case "LESS":     stencilSucces = ( ref & mask ) <  ( sTamp & mask ); raison = msgFunc("< "); break;
            case "LEQUAL":   stencilSucces = ( ref & mask ) <= ( sTamp & mask ); raison = msgFunc("<="); break;
            case "GREATER":  stencilSucces = ( ref & mask ) >  ( sTamp & mask ); raison = msgFunc("> "); break;
            case "GEQUAL":   stencilSucces = ( ref & mask ) >= ( sTamp & mask ); raison = msgFunc(">="); break;
            case "EQUAL":    stencilSucces = ( ref & mask ) == ( sTamp & mask ); raison = msgFunc("=="); break;
            case "NOTEQUAL": stencilSucces = ( ref & mask ) != ( sTamp & mask ); raison = msgFunc("!="); break;
            case "ALWAYS":   stencilSucces = true; raison = "toujours"; break;
            }
            deviendraPixel &= stencilSucces; // le fragment pourra devenir un pixel ?
            if ( stencilSucces )
                resultatSucces('stencilSucces','stencilMessage', "Le fragment passe ce test. Raison : " + raison + " &nbsp;&rArr;&nbsp; " + stencilSucces + ".");
            else
                resultatEchec('stencilSucces','stencilMessage', "Le fragment ne passe pas ce test et il n'ira pas plus loin. Raison : " + raison + " &nbsp;&rArr;&nbsp; " + stencilSucces + ".");
        }
        else
            resultatNonApplicable('stencilSucces','stencilMessage'); // ne se produira pas dans cet exemple
    }
    else
        resultatDesactive('stencilSucces','stencilMessage');

    //
    // test de profondeur
    //
    var depthSucces = true; // par défaut
    var depthEnabled = document.getElementById('depthEnabled').value;
    document.getElementById('depthfunc').disabled = !( depthEnabled === "Enable" );
    if ( depthEnabled === "Enable" )
    {
        if ( deviendraPixel )
        {
            var depthfunc = document.getElementById('depthfunc').value;
	    var raison = ""; // la raison du succès ou de l'insuccès du test
	    function msgFunc(test = "") { return "(zFrag " + test + " zTamp) &nbsp;&rArr;&nbsp; (" + zFrag + " " + test + " " + zTamp + ")"; }
            switch ( depthfunc ) {
            case "NEVER":    depthSucces = false; raison = "jamais"; break;
            case "LESS":     depthSucces = zFrag <  zTamp; raison = msgFunc("< "); break;
            case "LEQUAL":   depthSucces = zFrag <= zTamp; raison = msgFunc("<="); break;
            case "GREATER":  depthSucces = zFrag >  zTamp; raison = msgFunc("> "); break;
            case "GEQUAL":   depthSucces = zFrag >= zTamp; raison = msgFunc(">="); break;
            case "EQUAL":    depthSucces = zFrag == zTamp; raison = msgFunc("=="); break;
            case "NOTEQUAL": depthSucces = zFrag != zTamp; raison = msgFunc("!="); break;
            case "ALWAYS":   depthSucces = true; raison = "toujours"; break;
            }
            deviendraPixel &= depthSucces; // le fragment pourra devenir un pixel ?
            if ( depthSucces )
                resultatSucces('depthSucces','depthMessage', "Le fragment passe ce test. Raison : " + raison + " &nbsp;&rArr;&nbsp; " + depthSucces + ".");
            else
                resultatEchec('depthSucces','depthMessage', "Le fragment ne passe pas ce test et il n'ira pas plus loin. Raison : " + raison + "&nbsp;&rArr;&nbsp; " + depthSucces + ".");
        }
        else
            resultatNonApplicable('depthSucces','depthMessage');
    }
    else
        resultatDesactive('depthSucces','depthMessage',"(Le tampon de profondeur ne sera pas modifié.)");

    //
    // appliquer l'opération sur le stencil selon les résultats du test de stencil et du test de profondeur
    //
    if ( stencilEnabled === "Enable" )
    {
        var sfail = document.getElementById('sfail').value;
        var zfail = document.getElementById('zfail').value;
        var pass = document.getElementById('pass').value;
        if ( !stencilSucces )
        {
            switch ( sfail ) {
            case "KEEP":    sCour = sTamp; break;
            case "ZERO":    sCour = 0; break;
            case "REPLACE": sCour = ref & mask; break;
            case "INCR":    sCour = sTamp; sCour++; break;
            case "DECR":    sCour = sTamp; sCour--; break;
            case "INVERT":  sCour = -sTamp; break;
            }
        }
        else if ( !depthSucces )
        {
            switch ( zfail ) {
            case "KEEP":    sCour = sTamp; break;
            case "ZERO":    sCour = 0; break;
            case "REPLACE": sCour = ref & mask; break;
            case "INCR":    sCour = sTamp; sCour++; break;
            case "DECR":    sCour = sTamp; sCour--; break;
            case "INVERT":  sCour = -sTamp; break;
            }
        }
        else
        {
            switch ( pass ) {
            case "KEEP":    sCour = sTamp; break;
            case "ZERO":    sCour = 0; break;
            case "REPLACE": sCour = ref & mask; break;
            case "INCR":    sCour = sTamp; sCour++; break;
            case "DECR":    sCour = sTamp; sCour--; break;
            case "INVERT":  sCour = -sTamp; break;
            }
        }
    }

    //
    // fusion de couleurs
    //
    var blendEnabled = document.getElementById('blendEnabled').value;
    document.getElementById('sfactor').disabled = !( blendEnabled === "Enable" );
    document.getElementById('dfactor').disabled = !( blendEnabled === "Enable" );
    if ( blendEnabled === "Enable" )
    {
        if ( deviendraPixel )
        {
            var sfactor = document.getElementById('sfactor').value;
            var dfactor = document.getElementById('dfactor').value;
            var sr = 0.0, sg = 0.0, sb = 0.0, sa = 0.0;
            var dr = 0.0, dg = 0.0, db = 0.0, da = 0.0;
            switch ( sfactor ) {
            case "GL_ZERO":
                sr = ( 0.0 ) * rFrag;
                sg = ( 0.0 ) * gFrag;
                sb = ( 0.0 ) * bFrag;
                sa = ( 0.0 ) * aFrag;
                break;
            case "GL_ONE":
                sr = ( 1.0 ) * rFrag;
                sg = ( 1.0 ) * gFrag;
                sb = ( 1.0 ) * bFrag;
                sa = ( 1.0 ) * aFrag;
                break;
            case "GL_SRC_COLOR":
                sr = ( rFrag ) * rFrag;
                sg = ( gFrag ) * gFrag;
                sb = ( bFrag ) * bFrag;
                sa = ( aFrag ) * aFrag;
                break;
            case "GL_ONE_MINUS_SRC_COLOR":
                sr = ( 1.0 - rFrag ) * rFrag;
                sg = ( 1.0 - gFrag ) * gFrag;
                sb = ( 1.0 - bFrag ) * bFrag;
                sa = ( 1.0 - aFrag ) * aFrag;
                break;
            case "GL_DST_COLOR":
                sr = ( rTamp ) * rFrag;
                sg = ( gTamp ) * gFrag;
                sb = ( bTamp ) * bFrag;
                sa = ( aTamp ) * aFrag;
                break;
            case "GL_ONE_MINUS_DST_COLOR":
                sr = ( 1.0 - rTamp ) * rFrag;
                sg = ( 1.0 - gTamp ) * gFrag;
                sb = ( 1.0 - bTamp ) * bFrag;
                sa = ( 1.0 - aTamp ) * aFrag;
                break;
            case "GL_SRC_ALPHA":
                sr = ( aFrag ) * rFrag;
                sg = ( aFrag ) * gFrag;
                sb = ( aFrag ) * bFrag;
                sa = ( aFrag ) * aFrag;
                break;
            case "GL_ONE_MINUS_SRC_ALPHA":
                sr = ( 1.0 - aFrag ) * rFrag;
                sg = ( 1.0 - aFrag ) * gFrag;
                sb = ( 1.0 - aFrag ) * bFrag;
                sa = ( 1.0 - aFrag ) * aFrag;
                break;
            case "GL_DST_ALPHA":
                sr = ( aTamp ) * rFrag;
                sg = ( aTamp ) * gFrag;
                sb = ( aTamp ) * bFrag;
                sa = ( aTamp ) * aFrag;
                break;
            case "GL_ONE_MINUS_DST_ALPHA":
                sr = ( 1.0 - aTamp ) * rFrag;
                sg = ( 1.0 - aTamp ) * gFrag;
                sb = ( 1.0 - aTamp ) * bFrag;
                sa = ( 1.0 - aTamp ) * aFrag;
                break;
            }
            switch ( dfactor ) {
            case "GL_ZERO":
                dr = ( 0.0 ) * rTamp;
                dg = ( 0.0 ) * gTamp;
                db = ( 0.0 ) * bTamp;
                da = ( 0.0 ) * aTamp;
                break;
            case "GL_ONE":
                dr = ( 1.0 ) * rTamp;
                dg = ( 1.0 ) * gTamp;
                db = ( 1.0 ) * bTamp;
                da = ( 1.0 ) * aTamp;
                break;
            case "GL_SRC_COLOR":
                dr = ( rFrag ) * rTamp;
                dg = ( gFrag ) * gTamp;
                db = ( bFrag ) * bTamp;
                da = ( aFrag ) * aTamp;
                break;
            case "GL_ONE_MINUS_SRC_COLOR":
                dr = ( 1.0 - rFrag ) * rTamp;
                dg = ( 1.0 - gFrag ) * gTamp;
                db = ( 1.0 - bFrag ) * bTamp;
                da = ( 1.0 - aFrag ) * aTamp;
                break;
            case "GL_DST_COLOR":
                dr = ( rTamp ) * rTamp;
                dg = ( gTamp ) * gTamp;
                db = ( bTamp ) * bTamp;
                da = ( aTamp ) * aTamp;
                break;
            case "GL_ONE_MINUS_DST_COLOR":
                dr = ( 1.0 - rTamp ) * rTamp;
                dg = ( 1.0 - gTamp ) * gTamp;
                db = ( 1.0 - bTamp ) * bTamp;
                da = ( 1.0 - aTamp ) * aTamp;
                break;
            case "GL_SRC_ALPHA":
                dr = ( aFrag ) * rTamp;
                dg = ( aFrag ) * gTamp;
                db = ( aFrag ) * bTamp;
                da = ( aFrag ) * aTamp;
                break;
            case "GL_ONE_MINUS_SRC_ALPHA":
                dr = ( 1.0 - aFrag ) * rTamp;
                dg = ( 1.0 - aFrag ) * gTamp;
                db = ( 1.0 - aFrag ) * bTamp;
                da = ( 1.0 - aFrag ) * aTamp;
                break;
            case "GL_DST_ALPHA":
                dr = ( aTamp ) * rTamp;
                dg = ( aTamp ) * gTamp;
                db = ( aTamp ) * bTamp;
                da = ( aTamp ) * aTamp;
                break;
            case "GL_ONE_MINUS_DST_ALPHA":
                dr = ( 1.0 - aTamp ) * rTamp;
                dg = ( 1.0 - aTamp ) * gTamp;
                db = ( 1.0 - aTamp ) * bTamp;
                da = ( 1.0 - aTamp ) * aTamp;
                break;
            }
            rCour = clamp( sr+dr, 0.0, 1.0 );
            gCour = clamp( sg+dg, 0.0, 1.0 );
            bCour = clamp( sb+db, 0.0, 1.0 );
            aCour = clamp( sa+da, 0.0, 1.0 );
            resultatSucces('blendSucces','blendMessage','Couleurs fusionnées.');
        }
        else
            resultatNonApplicable('blendSucces','blendMessage');
    }
    else
        resultatDesactive('blendSucces','blendMessage');

    //
    // opérations logiques
    //
    // var logicEnabled = document.getElementById('logicEnabled').value;
    // document.getElementById('opcode').disabled = !( logicEnabled === "Enable" );
    // if ( logicEnabled === "Enable" )
    // {
    //  if ( deviendraPixel )
    //  {
    //         var opcode = document.getElementById('opcode').value;
    //         switch ( opcode ) {
    //      case "GL_CLEAR":
    //          rCour = 0.0;
    //          gCour = 0.0;
    //          bCour = 0.0;
    //          aCour = 0.0;
    //          break;
    //      case "GL_SET":
    //          rCour = 1.0;
    //          gCour = 1.0;
    //          bCour = 1.0;
    //          aCour = 1.0;
    //          break;
    //      case "GL_COPY":
    //          rCour = rCour;
    //          gCour = gCour;
    //          bCour = bCour;
    //          aCour = aCour;
    //          break;
    //      case "GL_COPY_INVERTED":
    //          rCour = 1-rCour;
    //          gCour = 1-gCour;
    //          bCour = 1-bCour;
    //          aCour = 1-aCour;
    //          break;
    //      case "GL_NOOP":
    //          rCour = rTamp;
    //          gCour = gTamp;
    //          bCour = bTamp;
    //          aCour = aTamp;
    //          break;
    //      case "GL_INVERT":
    //          rCour = 1-rTamp;
    //          gCour = 1-gTamp;
    //          bCour = 1-bTamp;
    //          aCour = 1-aTamp;
    //          break;
    //      case "GL_AND":
    //          rCour = rCour & rTamp;
    //          gCour = gCour & gTamp;
    //          bCour = bCour & bTamp;
    //          aCour = aCour & aTamp;
    //          break;
    //      case "GL_NAND":
    //          rCour = 1-( rCour & rTamp );
    //          gCour = 1-( gCour & gTamp );
    //          bCour = 1-( bCour & bTamp );
    //          aCour = 1-( aCour & aTamp );
    //          break;
    //      case "GL_OR":
    //          rCour = rCour | rTamp;
    //          gCour = gCour | gTamp;
    //          bCour = bCour | bTamp;
    //          aCour = aCour | aTamp;
    //          break;
    //      case "GL_NOR":
    //          rCour = 1-( rCour | rTamp );
    //          gCour = 1-( gCour | gTamp );
    //          bCour = 1-( bCour | bTamp );
    //          aCour = 1-( aCour | aTamp );
    //          break;
    //      case "GL_XOR":
    //          //rCour = (( (parseInt(rCour*256)%255) ^ (parseInt(rTamp*256)&255) ) & 255 )/256;
    //          rCour = rCour ^ rTamp;
    //          gCour = gCour ^ gTamp;
    //          bCour = bCour ^ bTamp;
    //          aCour = aCour ^ aTamp;
    //          break;
    //      case "GL_EQUIV":
    //          rCour = 1-( rCour ^ rTamp );
    //          gCour = 1-( gCour ^ gTamp );
    //          bCour = 1-( bCour ^ bTamp );
    //          aCour = 1-( aCour ^ aTamp );
    //          break;
    //      case "GL_AND_REVERSE":
    //          rCour = rCour & ~rTamp;
    //          gCour = gCour & ~gTamp;
    //          bCour = bCour & ~bTamp;
    //          aCour = aCour & ~aTamp;
    //          break;
    //      case "GL_AND_INVERTED":
    //          rCour = ~rCour & rTamp;
    //          gCour = ~gCour & gTamp;
    //          bCour = ~bCour & bTamp;
    //          aCour = ~aCour & aTamp;
    //          break;
    //      case "GL_OR_REVERSE":
    //          rCour = rCour | ~rTamp;
    //          gCour = gCour | ~gTamp;
    //          bCour = bCour | ~bTamp;
    //          aCour = aCour | ~aTamp;
    //          break;
    //      case "GL_OR_INVERTED":
    //          rCour = ~rCour | rTamp;
    //          gCour = ~gCour | gTamp;
    //          bCour = ~bCour | bTamp;
    //          aCour = ~aCour | aTamp;
    //          break;
    //         }
    //         resultatSucces('logicSucces','logicMessage','Opération appliquée.');
    //  }
    //  else
    //         resultatNonApplicable('logicSucces','logicMessage');
    // }
    // else
    //     resultatDesactive('logicSucces','logicMessage');

    //
    // FIN: tous les tests sont passés; assigner les valeurs aux tampons
    //
    if ( deviendraPixel )
    {
        document.getElementById('zFin').value = ( depthEnabled === "Enable" ) ? zCour : zTamp;
        document.getElementById('rFin').value = clamp( rCour, 0.0, 1.0 );
        document.getElementById('gFin').value = clamp( gCour, 0.0, 1.0 );
        document.getElementById('bFin').value = clamp( bCour, 0.0, 1.0 );
        document.getElementById('aFin').value = clamp( aCour, 0.0, 1.0 );
    }
    else
    {
        document.getElementById('zFin').value = zTamp;
        document.getElementById('rFin').value = clamp( rTamp, 0.0, 1.0 );
        document.getElementById('gFin').value = clamp( gTamp, 0.0, 1.0 );
        document.getElementById('bFin').value = clamp( bTamp, 0.0, 1.0 );
        document.getElementById('aFin').value = clamp( aTamp, 0.0, 1.0 );
    }
    document.getElementById('sFin').value = sCour;
}

function initPixel()
{
    document.getElementById('zFrag').value = 0.2;
    document.getElementById('rFrag').value = 0.9;
    document.getElementById('gFrag').value = 0.7;
    document.getElementById('bFrag').value = 0.5;
    document.getElementById('aFrag').value = 0.3;

    document.getElementById('zTamp').value = 0.6;
    document.getElementById('rTamp').value = 0.8;
    document.getElementById('gTamp').value = 0.8;
    document.getElementById('bTamp').value = 0.8;
    document.getElementById('aTamp').value = 0.8;
    document.getElementById('sTamp').value = 1;

    // intra
    document.getElementById('zFrag').value = 0.1;
    document.getElementById('rFrag').value = 0.9;
    document.getElementById('gFrag').value = 0.7;
    document.getElementById('bFrag').value = 0.5;
    document.getElementById('aFrag').value = 0.3;

    document.getElementById('zTamp').value = 0.7;
    document.getElementById('rTamp').value = 0.6;
    document.getElementById('gTamp').value = 0.6;
    document.getElementById('bTamp').value = 0.6;
    document.getElementById('aTamp').value = 0.6;
    document.getElementById('sTamp').value = 5;
    document.getElementById('ref').value = 1;
    document.getElementById('mask').value = 7;
    document.getElementById('sfail').value = "INCR";
    document.getElementById('zfail').value = "DECR";
    document.getElementById('pass').value = "REPLACE";

    calculerPixel();
}

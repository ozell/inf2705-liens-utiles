




/*
     FILE ARCHIVED ON 7:53:58 nov. 17, 2007 AND RETRIEVED FROM THE
     INTERNET ARCHIVE ON 16:16:46 oct. 28, 2011.
     JAVASCRIPT APPENDED BY WAYBACK MACHINE, COPYRIGHT INTERNET ARCHIVE.

     ALL OTHER CONTENT MAY ALSO BE PROTECTED BY COPYRIGHT (17 U.S.C.
     SECTION 108(a)(3)).
*/
   var formHasBeenSaved=0;
   var tmpIframeStatus = new Array();
   
   function implodeParam(istr, param)
   {
      var xstr = istr;
      var i=0;
      var xval;
      var e;
      var dpi;
      var dpe;
      while (i>=0 && i<param.length)
      {
         e=param.indexOf('=', i);
         if (e==-1) break;
         xname=param.substr(i, e-i);
         i=e+1;
         e=param.indexOf(',', i);
         if (e==-1) 
         {
            xval=param.substr(i)
         }
         else 
         {
            xval=param.substr(i, e-i);  
         }
         dpi=0;
         do 
         {
            dpi=xstr.indexOf(xname+'=', dpi);
            if ((dpi==0) || ((dpi>0) && (xstr.substr(dpi-1,1)==','))) break;
            if (dpi>0) dpi++;
         } while (dpi!=-1);
         
         if (dpi==-1) xstr = xstr + ','+xname+'='+xval;
         else
         {
            dpi+=xname.length+1;
            dpe=xstr.indexOf(',', dpi);
            if (dpe==-1) dpe=xstr.length;
            xstr=strDelete(xstr, dpi, dpe-1, xval);
         }
         i=e;
         if (i==-1) break;
         i++;
      }
      return xstr;
   }
   
   function newWindowA(pageName, p)
   {
      var xstr = implodeParam('scrollbars=yes,toolbar=no,status=no,menubar=no,directories=no,location=no,resizable=yes,width=600,height=420', p);
      var prodWindow = window.open(pageName, 'window'+Math.round(Math.random()*1000), xstr);
      if (prodWindow) prodWindow.focus();
   }

   function newWindowB(pageName, p, winame)
   {
      var xstr = implodeParam('scrollbars=yes,toolbar=no,status=no,menubar=no,directories=no,location=no,resizable=yes,width=600,height=420', p);
      //alert(xstr);
      open(pageName, winame, xstr);
   }

   function strDelete(xstr, i0, i1, s)
   {
      return xstr.substring(0, i0)+s+xstr.substring(i1+1);
   }

   function cc(w, h)
   {
      var ktop=(screen.availHeight-h)/2;
      var kleft=(screen.availWidth-w)/2;
      return 'top='+ktop+',left='+kleft+',width='+w+',height='+h;
   }
   
   function maximumDimension()
   {
      var ktop=(screen.availHeight)/2;
      var kleft=(screen.availWidth)/2;
      return 'top='+ktop+',left='+kleft+',width='+screen.availWidth+',height='+screen.availHeight;
   }
   
   function msg_su()
   {
      if (formHasBeenSaved) newWindowA('sumsg.html', cc(300, 100));
   }

   function confirmDeletePhoto(s) 
   {
      if (!s) s = 'photo';
     if (confirm('Are you sure you want to delete this '+s+'?')) return true;
     return false;
   }

   function confirmDelete(s) 
   {
      if (!s) s = 'record(s)';
     if (confirm('Are you sure you want to delete this '+s+'?')) return true;
     return false;
   }

   function nsAlert(msg)
   {
      var p = 'scrollbars=yes,toolbar=no,status=no,menubar=no,directories=no,location=no,resizable=yes,width=400,height=100';
      window.open('msgbox.php?msg='+msg, 'Message', implodeParam(p, cc(350,375)));
   }

   function submitDelete(fm)
   {
      if (!fm) fm="fm";
      var f=document[fm];
      f.fmDeleteRecords.value = "1"
      f.submit();
   }
   
   function submitDefaultForm(fg)
   {
      var f=document.fm;
      f[fg].value = "1"
      f.submit();
   }
   
   function showSection(j)
   {
      re = /^section_/
     for(i=0; (a = document.getElementsByTagName("div")[i]); i++) 
     {
         if (re.test(a.id)) 
         {
            a.style.display = (("section_"+j) == a.id) ? 'block' : 'none';
         }
     }
   }
   function getContentOf(url, i)
   {
      if (!i) i = "tmpiframe"
      tmpIframeStatus.push(i)
      var i = document.getElementById(i)
      i.src = url
   }

   function setContentOf(i, from)
   {
      
      if (!from) from = "tmpiframe"
      var fg = -1;

      for (k=0; k<tmpIframeStatus.length; k++)
      {
         if (tmpIframeStatus[k] == from) 
         {
            fg = k;
         }
      }
      if (fg < 0) 
      {
         return true;
      }
      
      tmpIframeStatus.splice(fg, 1)
      var IFrameObj = document.getElementById(from)

      var t = document.getElementById(i)

      if (IFrameObj.contentDocument) 
      {
         // For NS6
         IFrameDoc = IFrameObj.contentDocument; 
      } 
      else if (IFrameObj.contentWindow) 
      {
         // For IE5.5 and IE6
         IFrameDoc = IFrameObj.contentWindow.document;
      } 
      else if (IFrameObj.document) 
      {
         // For IE5
         IFrameDoc = IFrameObj.document;
      } 
      else 
      {
         return true;
      }

      t.innerHTML = IFrameDoc.body.innerHTML
   }
